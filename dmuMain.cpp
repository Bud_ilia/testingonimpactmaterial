//---------------------------------------------------------------------------


#pragma hdrstop

#include "dmuMain.h"
#include "uUntilsForTest.h"
#include "fmuKnowledge.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "FMX.Controls.TControl"
#pragma resource "*.dfm"
Tdm *dm;
//---------------------------------------------------------------------------
__fastcall Tdm::Tdm(TComponent* Owner)
	: TDataModule(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tdm::Action1Execute(TObject *Sender)
{
	//
}
//---------------------------------------------------------------------------


void __fastcall Tdm::tmVideoProgressPlayTimer(TObject *Sender)
{
fmKnowledge->tbChengingProcess->Tag=1;
fmKnowledge->tbChengingProcess->Value=mpVideo->CurrentTime;
fmKnowledge->tbChengingProcess->Tag=0;
fmKnowledge-> laTimeChanged->Text=Format("%2.2d:%2.2d",ARRAYOFCONST((
(int) mpVideo->CurrentTime/MediaTimeScale/60,
(int)mpVideo->CurrentTime/MediaTimeScale%60
)));
}
//---------------------------------------------------------------------------

