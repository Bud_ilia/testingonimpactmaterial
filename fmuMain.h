//---------------------------------------------------------------------------

#ifndef fmuMainH
#define fmuMainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Layouts.hpp>
//---------------------------------------------------------------------------
class TfmMenu : public TForm
{
__published:	// IDE-managed Components
	TToolBar *tbMenu;
	TLabel *laCaptionMenu;
	TButton *buExit;
	TLayout *ly;
	TGridPanelLayout *gpl;
	TButton *buKnowledge;
	TButton *buProcessingData;
	TButton *buAbout;
	void __fastcall buExitClick(TObject *Sender);
	void __fastcall ShowKnowledge(TObject *Sender);
	void __fastcall ShowAbout(TObject *Sender);
	void __fastcall ShowProccessingData(TObject *Sender);
	//void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TfmMenu(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfmMenu *fmMenu;
//---------------------------------------------------------------------------
#endif
