//---------------------------------------------------------------------------

#ifndef fmuAboutH
#define fmuAboutH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.ImgList.hpp>
//---------------------------------------------------------------------------
class TfmAbout : public TForm
{
__published:	// IDE-managed Components
	TToolBar *tbAbout;
	TButton *buReturn;
	TLabel *laCaptionAbout;
	TMemo *mAbout;
	TGlyph *gAbout;
	void __fastcall buReturnClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TfmAbout(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfmAbout *fmAbout;
//---------------------------------------------------------------------------
#endif
