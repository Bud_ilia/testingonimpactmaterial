//---------------------------------------------------------------------------

#ifndef fmuCalcH
#define fmuCalcH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.ImgList.hpp>
#include <FMX.Objects.hpp>
#include <System.Ioutils.hpp>
#include <FMX.ListBox.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <System.Math.hpp>
#include <FMX.Media.hpp>
#include <System.IOUtils.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
using namespace std;
//---------------------------------------------------------------------------
const UnicodeString cList[]=
 {
 "Order","Instrument"
 };
 const UnicodeString usWay =  "..\\..\\Impact\\";
//---------------------------------------------------------------------------

class TfmCalculator : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tbMain;
	TTabItem *tiMain;
	TTabItem *tiCheck;
	TToolBar *tbInMain;
	TButton *buReturnToMenu;
	TLabel *laCaptionMain;
	TGridPanelLayout *gpl;
	TButton *buChecking;
	TToolBar *tbCheck;
	TButton *buReturn;
	TLabel *laCaptionCheck;
	TGlyph *g1;
	TLayout *ly;
	TTabItem *tiOrder;
	TToolBar *tb;
	TButton *buBack;
	TLabel *laInfoOrder;
	TScrollBox *sbOrder;
	TText *tOrder;
	TGlyph *gOrder;
	TButton *buInfo;
	TTabItem *tiInstrument;
	TToolBar *tbInstrument;
	TButton *buRet;
	TLabel *laCaptionInstrument;
	TGlyph *gInstrument;
	TScrollBox *sbInstrument;
	TText *tInstrument;
	TToolBar *tbManageImageOrder;
	TButton *buPrev;
	TButton *buNext;
	TLabel *laInfo;
	TToolBar *tbLabels;
	TLabel *laMaterial;
	TComboBox *cbMaterial;
	TLabel *laWidth;
	TLabel *laHeight;
	TLabel *laSection;
	TLabel *laWork;
	TLabel *laViscosity;
	TEdit *edWidth;
	TEdit *edHeight;
	TEdit *edSection;
	TEdit *edWork;
	TEdit *edViscosity;
	TToolBar *tbButtom;
	TButton *buCheckNum;
	TMemo *mMessages;
	TButton *buVideo;
	TButton *buLab;
	TTabItem *tiVideo;
	TToolBar *tbVideo;
	TButton *buRetfr;
	TLabel *laCaptionVideo;
	TLayout *lyVideo;
	TToolBar *tbTimeVideo;
	TLabel *laTime;
	TLabel *laTimeConst;
	TTrackBar *tbVideoChanging;
	TMediaPlayerControl *mpc;
	TToolBar *tbVideoInstruments;
	TButton *buPlay;
	TButton *buPause;
	TButton *buStop;
	TTimer *tm;
	TMediaPlayer *mp;
	TListView *lv;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buReturnToMenuClick(TObject *Sender);
	void __fastcall buCheckingClick(TObject *Sender);
	void __fastcall buReturnClick(TObject *Sender);
	void __fastcall buLabClick(TObject *Sender);
	void __fastcall buInfoClick(TObject *Sender);
	void __fastcall buPrevClick(TObject *Sender);
	void __fastcall buNextClick(TObject *Sender);
	void __fastcall edWidthChange(TObject *Sender);
	void __fastcall edHeightChange(TObject *Sender);
	void __fastcall edSectionChange(TObject *Sender);
	void __fastcall edWorkChange(TObject *Sender);
	void __fastcall edViscosityChange(TObject *Sender);
	void __fastcall buCheckNumClick(TObject *Sender);
	void __fastcall buVideoClick(TObject *Sender);
	void __fastcall buRetfrClick(TObject *Sender);
	void __fastcall tmTimer(TObject *Sender);
	void __fastcall buPlayClick(TObject *Sender);
	void __fastcall buPauseClick(TObject *Sender);
	void __fastcall tbVideoChangingChange(TObject *Sender);
	void __fastcall buStopClick(TObject *Sender);
	void __fastcall lvItemClick(TObject * const Sender, TListViewItem * const AItem);

private:	// User declarations
void LoadItem(int aIndex);
UnicodeString FPath;
void LoadItem_1(int aIndex);
//��� ��������
float iHeight;
float iSection;
float iViscosity;
float iWidth;
float iWork;
float ChSection;
float ChViscosity;
//double ChWork;

unsigned int FPauseTime;
void DoRefreshList(UnicodeString aPath);
void DoStop();

public:		// User declarations
	__fastcall TfmCalculator(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfmCalculator *fmCalculator;
//---------------------------------------------------------------------------
#endif
