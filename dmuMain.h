//---------------------------------------------------------------------------

#ifndef dmuMainH
#define dmuMainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.ActnList.hpp>
#include <FMX.ImgList.hpp>
#include <System.Actions.hpp>
#include <System.ImageList.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Media.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Gestures.hpp>
//---------------------------------------------------------------------------
class Tdm : public TDataModule
{
__published:	// IDE-managed Components
	TImageList *il;
	TImageList *ilAdditional;
	TMediaPlayer *mpVideo;
	TTimer *tmVideoProgressPlay;
	TStyleBook *sbStyleLibrary;
	TImageList *ilInstrument;
	void __fastcall Action1Execute(TObject *Sender);
	void __fastcall tmVideoProgressPlayTimer(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Tdm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tdm *dm;
//---------------------------------------------------------------------------
#endif
