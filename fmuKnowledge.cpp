//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmuKnowledge.h"
#include "dmuMain.h"
#include "uUntilsForTest.h"
#include "fmuMain.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TfmKnowledge *fmKnowledge;
//---------------------------------------------------------------------------
__fastcall TfmKnowledge::TfmKnowledge(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
const UnicodeString cList[]=
 {
 "info"
 };
//---------------------------------------------------------------------------
 void TfmKnowledge::LoadItem(int aIndex)
{
UnicodeString xName=System::Ioutils::TPath::Combine(FPath,cList[aIndex]);
TStringList* x=new TStringList;
__try
{
x->LoadFromFile(xName+".txt");
tInfo->Text=x->Text;
}
__finally
{
x->DisposeOf();
}

}
//---------------------------------------------------------------------------
void __fastcall TfmKnowledge::FormCreate(TObject *Sender)
{
FPath=
 #ifdef _Windows
 "..\\..\\inc\\";
 #else
 System::Ioutils::TPath::GetDocumentsPath();
 #endif
tc->ActiveTab=tiMenu;
tcTest->ActiveTab=tiBegginning;
gImage->ImageIndex=0;
LoadItem(0);
DoRefreshList(System::Ioutils::TPath::GetFullPath(cWay),gp3);
FPauseTime=0;
}
//---------------------------------------------------------------------------

void __fastcall TfmKnowledge::buGoAwayClick(TObject *Sender)
{
fmMenu->Visible=true;
Close();
}
//---------------------------------------------------------------------------

void __fastcall TfmKnowledge::lbiKnowledgeClick(TObject *Sender)
{
   tc->ActiveTab=tiKnowledge;
}
//---------------------------------------------------------------------------

void __fastcall TfmKnowledge::lbiCheckingClick(TObject *Sender)
{
tc->ActiveTab=tiCheckingKnowledge;
}
//---------------------------------------------------------------------------

void __fastcall TfmKnowledge::ReturnToMenu(TObject *Sender)
{
tc->ActiveTab=tiMenu;
DoStop();
}
//---------------------------------------------------------------------------

void __fastcall TfmKnowledge::buBeginClick(TObject *Sender)
{
tcTest->ActiveTab=tiQuest1 ;
}
//---------------------------------------------------------------------------

void __fastcall TfmKnowledge::buRepeatTestClick(TObject *Sender)
{
	 tcTest->ActiveTab=tiBegginning;
     mEnd->Lines->Clear();
}
//---------------------------------------------------------------------------


void __fastcall TfmKnowledge::buBackwardsClick(TObject *Sender)
{
   gImage->ImageIndex--;
   if(gImage->ImageIndex<0)gImage->ImageIndex=5;

	  switch(gImage->ImageIndex)
	  {
	  case 0:laInfo->Text="���.1. �������";break;
	  case 1:laInfo->Text="���.2.����������� ������� �������� ����� �� ����������� �������";break;
	  case 2:laInfo->Text="���.3. ����� ������������ �����";break;
	  case 3:laInfo->Text="���.4. ������� � U-�������� �������� ��� ��������� �� ������� �����";break;
	  case 4:laInfo->Text="���. 5. ����� ��������� ��������: �) ������� �� ���������; ";break;
	  case 5:laInfo->Text="���. 5. ����� ��������� ��������:�) ������� �� ��������";break;
	  default:break;
	  }
}
//---------------------------------------------------------------------------

void __fastcall TfmKnowledge::buForwardsClick(TObject *Sender)
{

gImage->ImageIndex++;
	  if (gImage->ImageIndex>5) gImage->ImageIndex=0;

	  switch(gImage->ImageIndex)
	  {
	  case 0:laInfo->Text="���.1. �������";break;
	  case 1:laInfo->Text="���.2.����������� ������� �������� ����� �� ����������� �������";break;
	  case 2:laInfo->Text="���.3. ����� ������������ �����";break;
	  case 3:laInfo->Text="���.4. ������� � U-�������� �������� ��� ��������� �� ������� �����";break;
	  case 4:laInfo->Text="���. 5. ����� ��������� ��������: �) ������� �� ���������; ";break;
	  case 5:laInfo->Text="���. 5. ����� ��������� ��������:�) ������� �� ��������";break;
	  default:break;
	  }
}
//---------------------------------------------------------------------------

void __fastcall TfmKnowledge::lbiVideoClick(TObject *Sender)
{
tc->ActiveTab=tiVideo;

}
//---------------------------------------------------------------------------

void __fastcall TfmKnowledge::buQuestion1Answ1Click(TObject *Sender)
{
mEnd->Lines->Add("1 ������-�����������");
tcTest->ActiveTab=tiQuest2;
}
//---------------------------------------------------------------------------

void __fastcall TfmKnowledge::buQuestion1Answ2Click(TObject *Sender)
{
mEnd->Lines->Add("1 ������-���������");
tcTest->ActiveTab=tiQuest2;
}
//---------------------------------------------------------------------------

void __fastcall TfmKnowledge::buQuestion2Answ1Click(TObject *Sender)
{
mEnd->Lines->Add("2 ������-���������");
tcTest->ActiveTab=tiQuest3;
}
//---------------------------------------------------------------------------

void __fastcall TfmKnowledge::buQuestion2Answ2Click(TObject *Sender)
{
mEnd->Lines->Add("2 ������-�����������");
tcTest->ActiveTab=tiQuest3;
}
//---------------------------------------------------------------------------

void __fastcall TfmKnowledge::buQuestion3Answ3Click(TObject *Sender)
{
mEnd->Lines->Add("3 ������-���������");
tcTest->ActiveTab=laQuest4;
}
//---------------------------------------------------------------------------

void __fastcall TfmKnowledge::buQuestion3Answ1Click(TObject *Sender)
{
mEnd->Lines->Add("3 ������-�����������");
tcTest->ActiveTab=laQuest4;
}
//---------------------------------------------------------------------------

void __fastcall TfmKnowledge::buQuestion4Answ1Click(TObject *Sender)
{
 mEnd->Lines->Add("4 ������-���������");
tcTest->ActiveTab=tiQuest5;
}
//---------------------------------------------------------------------------

void __fastcall TfmKnowledge::buQuestion4Answ2Click(TObject *Sender)
{
mEnd->Lines->Add("4 ������-�����������");
tcTest->ActiveTab=tiQuest5;
}
//---------------------------------------------------------------------------

void __fastcall TfmKnowledge::buQuestion5Answ1Click(TObject *Sender)
{
 mEnd->Lines->Add("5 ������-���������");
tcTest->ActiveTab=laQuest6;
}
//---------------------------------------------------------------------------

void __fastcall TfmKnowledge::buQuestion5Answ2Click(TObject *Sender)
{
 mEnd->Lines->Add("5 ������-�����������");
tcTest->ActiveTab=laQuest6;
}
//---------------------------------------------------------------------------

void __fastcall TfmKnowledge::buQuestion6Answ1Click(TObject *Sender)
{
  mEnd->Lines->Add("6 ������-�����������");
tcTest->ActiveTab=tiQuest7;
}
//---------------------------------------------------------------------------

void __fastcall TfmKnowledge::buQuestion6Answ2Click(TObject *Sender)
{
 mEnd->Lines->Add("6 ������-���������");
tcTest->ActiveTab=tiQuest7;
}
//---------------------------------------------------------------------------

void __fastcall TfmKnowledge::buQuestion7Answ2Click(TObject *Sender)
{
 mEnd->Lines->Add("7 ������-�����������");
tcTest->ActiveTab=tiQuest8;
}
//---------------------------------------------------------------------------

void __fastcall TfmKnowledge::buQuestion7Answ1Click(TObject *Sender)
{
mEnd->Lines->Add("7 ������-���������");
tcTest->ActiveTab=tiQuest8;
}
//---------------------------------------------------------------------------

void __fastcall TfmKnowledge::buQuestion8Answ1Click(TObject *Sender)
{
  mEnd->Lines->Add("8 ������-�����������");
tcTest->ActiveTab=tiQuest9;
}
//---------------------------------------------------------------------------

void __fastcall TfmKnowledge::buQuestion8Answ3Click(TObject *Sender)
{
mEnd->Lines->Add("8 ������-���������");
tcTest->ActiveTab=tiQuest9;
}
//---------------------------------------------------------------------------

void __fastcall TfmKnowledge::buQuestion9Answ3Click(TObject *Sender)
{
 mEnd->Lines->Add("9 ������-���������");
tcTest->ActiveTab=tiQuest10;
}
//---------------------------------------------------------------------------

void __fastcall TfmKnowledge::buQuestion9Answ1Click(TObject *Sender)
{
   mEnd->Lines->Add("9 ������-�����������");
tcTest->ActiveTab=tiQuest10;
}
//---------------------------------------------------------------------------

void __fastcall TfmKnowledge::buQuestion10Answ1Click(TObject *Sender)
{
mEnd->Lines->Add("10 ������-�����������");
tcTest->ActiveTab=tiEnd;
}
//---------------------------------------------------------------------------

void __fastcall TfmKnowledge::buQuestion10Answ2Click(TObject *Sender)
{
	mEnd->Lines->Add("10 ������-���������");
tcTest->ActiveTab=tiEnd;



}

//---------------------------------------------------------------------------
void TfmKnowledge::DoRefreshList(UnicodeString aPath,const UnicodeString Sort)
{
laTimeVideo->Text=aPath;
DynamicArray<UnicodeString> x;
TListViewItem *xItem;
x=System::Ioutils::TDirectory::GetFiles(aPath, Sort);
lv->BeginUpdate();

try
{
	for(int i(0);i<x.Length;i++)
	{
	  xItem=lv->Items->Add();
	  xItem->Text=System::Ioutils::TPath::GetFileNameWithoutExtension(x[i]);
	  xItem->Detail=x[i];
	}
	}
	__finally
	{
	  lv->EndUpdate();
	}
}
//---------------------------------------------------------------------------
void __fastcall TfmKnowledge::buRefreshListClick(TObject *Sender)
{
lv->BeginUpdate();
lv->Items->Clear();
DoRefreshList(System::Ioutils::TPath::GetFullPath(cWay),gp3);
lv->EndUpdate();


}
//---------------------------------------------------------------------------
void TfmKnowledge::DoStop()
{
 dm->mpVideo->Stop();
dm->tmVideoProgressPlay->Enabled=false;
lv->ItemIndex=-1;
tbChengingProcess->Value=0;
FPauseTime=0;
}
//---------------------------------------------------------------------------

void __fastcall TfmKnowledge::buStopClick(TObject *Sender)
{


if(dm->mpVideo->State==TMediaState::Playing)DoStop();

}
//---------------------------------------------------------------------------

void __fastcall TfmKnowledge::buPauseClick(TObject *Sender)
{

if(dm->mpVideo->State==TMediaState::Playing)
{
FPauseTime=dm->mpVideo->Media->CurrentTime;
dm->mpVideo->Stop();
}
else
{
  buPlayClick(this);
}

}
//---------------------------------------------------------------------------

void __fastcall TfmKnowledge::buPlayClick(TObject *Sender)
{
	 if (lv->ItemIndex!=-1)
	{
		dm->tmVideoProgressPlay->Enabled=false;
		dm->mpVideo->Clear();
		dm->mpVideo->FileName=lv->Items->AppearanceItem[lv->ItemIndex]->Detail;
		tbChengingProcess->Enabled=true;
		tbChengingProcess->Max=dm->mpVideo->Duration;
		laTimeVideo->Text=Format("%2.2d;%2.2d",ARRAYOFCONST
		(( (unsigned int)dm->mpVideo->Duration/MediaTimeScale/60,
			(unsigned int)dm->mpVideo->Duration /MediaTimeScale%60
			)));
			dm->mpVideo->CurrentTime=FPauseTime;
			FPauseTime=0;
			dm->mpVideo->Play();
			dm->tmVideoProgressPlay->Enabled=true;
	}
}
//---------------------------------------------------------------------------

void __fastcall TfmKnowledge::tbChengingProcessChange(TObject *Sender)
{
   if(tbChengingProcess->Tag==0)
{
if((dm->mpVideo->State==TMediaState::Stopped)&&(FPauseTime!=0))
{
FPauseTime=tbChengingProcess->Value;
} else
{
dm->mpVideo->CurrentTime=tbChengingProcess->Value;
}
}
}
//---------------------------------------------------------------------------

void __fastcall TfmKnowledge::lvItemClick(TObject * const Sender, TListViewItem * const AItem)

{
 buPlayClick(this);


}
//---------------------------------------------------------------------------

void __fastcall TfmKnowledge::buPreviousClick(TObject *Sender)
{
if((lv->ItemIndex!=-1)&&(lv->ItemIndex>0))
{
lv->ItemIndex=lv->ItemIndex -1;
FPauseTime=0;
 buPlayClick(this);
}
else
{
DoStop();
}
}
//---------------------------------------------------------------------------

void __fastcall TfmKnowledge::buNextClick(TObject *Sender)
{
 if  ((lv->ItemIndex!=-1)&&(lv->ItemIndex<lv->ItemCount-1))
   {
   lv->ItemIndex=lv->ItemIndex+1;
   FPauseTime=0;
   buPlayClick(this);
   }
   else
   {
   DoStop();
   }
}
//---------------------------------------------------------------------------



