//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmuMain.h"
#include "fmuAbout.h"
#include "uUntilsForTest.h"
#include "dmuMain.h"
#include "fmuKnowledge.h"
#include "fmuCalc.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TfmMenu *fmMenu;
//---------------------------------------------------------------------------
__fastcall TfmMenu::TfmMenu(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------


void __fastcall TfmMenu::buExitClick(TObject *Sender)
{
Close();
}
//---------------------------------------------------------------------------

void __fastcall TfmMenu::ShowKnowledge(TObject *Sender)
{
 this->Visible=false;
  fmKnowledge->Show();
}
//---------------------------------------------------------------------------

void __fastcall TfmMenu::ShowAbout(TObject *Sender)
{
this->Visible=false;
fmAbout->Show();
}
//---------------------------------------------------------------------------

void __fastcall TfmMenu::ShowProccessingData(TObject *Sender)
{
this->Visible=false;
fmCalculator->Show();
}
//---------------------------------------------------------------------------

