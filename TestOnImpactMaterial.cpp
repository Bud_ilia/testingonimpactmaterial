//---------------------------------------------------------------------------

#include <fmx.h>
#ifdef _WIN32
#include <tchar.h>
#endif
#pragma hdrstop
#include <System.StartUpCopy.hpp>
//---------------------------------------------------------------------------
USEFORM("fmuMain.cpp", fmMenu);
USEFORM("dmuMain.cpp", dm); /* TDataModule: File Type */
USEFORM("fmuAbout.cpp", fmAbout);
USEFORM("fmuKnowledge.cpp", fmKnowledge);
USEFORM("fmuCalc.cpp", fmCalculator);
//---------------------------------------------------------------------------
extern "C" int FMXmain()
{
	try
	{
		Application->Initialize();
		Application->CreateForm(__classid(TfmMenu), &fmMenu);
		Application->CreateForm(__classid(Tdm), &dm);
		Application->CreateForm(__classid(TfmAbout), &fmAbout);
		Application->CreateForm(__classid(TfmKnowledge), &fmKnowledge);
		Application->CreateForm(__classid(TfmCalculator), &fmCalculator);
		Application->Run();
	}
	catch (Exception &exception)
	{
		Application->ShowException(&exception);
	}
	catch (...)
	{
		try
		{
			throw Exception("");
		}
		catch (Exception &exception)
		{
			Application->ShowException(&exception);
		}
	}
	return 0;
}
//---------------------------------------------------------------------------
