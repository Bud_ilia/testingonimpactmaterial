//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmuAbout.h"
#include "dmuMain.h"
#include "uUntilsForTest.h"
#include "fmuMain.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TfmAbout *fmAbout;
//---------------------------------------------------------------------------
__fastcall TfmAbout::TfmAbout(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TfmAbout::buReturnClick(TObject *Sender)
{
fmMenu->Visible=true;
Close();
}
//---------------------------------------------------------------------------

