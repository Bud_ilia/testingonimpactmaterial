//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmuCalc.h"
#include "dmuMain.h"
#include "fmuMain.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TfmCalculator *fmCalculator;
//---------------------------------------------------------------------------
__fastcall TfmCalculator::TfmCalculator(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TfmCalculator::FormCreate(TObject *Sender)
{
iHeight=0;
iSection=0;
iViscosity=0;
iWidth=0;
iWork=0;
FPauseTime=0;
ChSection=0;
ChViscosity=0;
//ChWork=0;

DoRefreshList(System::Ioutils::TPath::GetFullPath(usWay));

tbMain->ActiveTab= tiMain;
FPath=
 #ifdef _Windows
 "..\\..\\inc\\";
 #else
 System::Ioutils::TPath::GetDocumentsPath();
 #endif
 LoadItem(0);
 LoadItem_1(1);
}
//---------------------------------------------------------------------------
void __fastcall TfmCalculator::buReturnToMenuClick(TObject *Sender)
{
fmMenu->Visible=true;
Close();
}
//---------------------------------------------------------------------------
void __fastcall TfmCalculator::buCheckingClick(TObject *Sender)
{
 tbMain->ActiveTab= tiCheck;
}
//---------------------------------------------------------------------------

void __fastcall TfmCalculator::buReturnClick(TObject *Sender)
{
tbMain->ActiveTab= tiMain;
}
//---------------------------------------------------------------------------
 void TfmCalculator::LoadItem(int aIndex)
{
UnicodeString xName=System::Ioutils::TPath::Combine(FPath,cList[aIndex]);
TStringList* x=new TStringList;
__try
{
x->LoadFromFile(xName+".txt");
tOrder->Text=x->Text;
}
__finally
{
x->DisposeOf();
}

}
//---------------------------------------------------------------------------

void __fastcall TfmCalculator::buLabClick(TObject *Sender)
{
tbMain->ActiveTab= tiOrder;
}
//---------------------------------------------------------------------------
void TfmCalculator::LoadItem_1(int aIndex)
{
UnicodeString xName=System::Ioutils::TPath::Combine(FPath,cList[aIndex]);
//im->Bitmap->LoadFromFile(xName+".jpg");
TStringList* x=new TStringList;
__try
{
x->LoadFromFile(xName+".txt");
tInstrument->Text=x->Text;
}
__finally
{
x->DisposeOf();
}

}
//---------------------------------------------------------------------------
void __fastcall TfmCalculator::buInfoClick(TObject *Sender)
{
tbMain->ActiveTab=tiInstrument;
}
//---------------------------------------------------------------------------

void __fastcall TfmCalculator::buPrevClick(TObject *Sender)
{
gInstrument->ImageIndex--;
   if(gInstrument->ImageIndex<0)gInstrument->ImageIndex=1;

	  switch(gInstrument->ImageIndex)
	  {
	  case 0:laInfo->Text="���.1. ����������� ��������� �� �������";
	  break;
	  case 1:laInfo->Text="���.2. ���������� ������������� ��������������";
	  break;
	  default:break;
	  }

}
//---------------------------------------------------------------------------

void __fastcall TfmCalculator::buNextClick(TObject *Sender)
{
  gInstrument->ImageIndex++;
  if(gInstrument->ImageIndex>1)gInstrument->ImageIndex=0;

	  switch(gInstrument->ImageIndex)
	  {
	  case 0:laInfo->Text="���.1. ����������� ��������� �� �������";
	  break;
	  case 1:laInfo->Text="���.2. ���������� ������������� ��������������";
	  break;
	  default:break;
	  }

}
//---------------------------------------------------------------------------

void __fastcall TfmCalculator::edWidthChange(TObject *Sender)
{


//iWidth=StrToIntDef(edWidth->Text,1);
iWidth=StrToFloat(edWidth->Text);
}
//---------------------------------------------------------------------------

void __fastcall TfmCalculator::edHeightChange(TObject *Sender)
{

//iHeight=StrToIntDef(edHeight->Text,1);
iHeight=StrToFloat(edHeight->Text);
}
//---------------------------------------------------------------------------

void __fastcall TfmCalculator::edSectionChange(TObject *Sender)
{

//iSection=StrToIntDef(edSection->Text,1);
iSection=StrToFloat(edSection->Text);
}
//---------------------------------------------------------------------------

void __fastcall TfmCalculator::edWorkChange(TObject *Sender)
{

//iWork=StrToIntDef(edWork->Text,1);
iWork=StrToFloat(edWork->Text);
}
//---------------------------------------------------------------------------

void __fastcall TfmCalculator::edViscosityChange(TObject *Sender)
{

//iViscosity=StrToFloat(edViscosity->Text);
iViscosity=floor(StrToFloat(edViscosity->Text));
}
//---------------------------------------------------------------------------

void __fastcall TfmCalculator::buCheckNumClick(TObject *Sender)
{
mMessages->Lines->Clear();

ChSection=iWidth*iHeight;

if(iSection!=ChSection)
{
mMessages->Lines->Add("������� ����������� ������� ���������� �����������");
}
else mMessages->Lines->Add("������� ����������� ������� ���������� ���������");

ChViscosity=floor(iWork/ChSection);

if (iViscosity!=ChViscosity) {
 mMessages->Lines->Add("�������� ������� �������� ���������� �����������");
}
else  mMessages->Lines->Add("�������� ������� �������� ���������� ���������");
//ChWork=0;
}
//---------------------------------------------------------------------------

void __fastcall TfmCalculator::buVideoClick(TObject *Sender)
{
tbMain->ActiveTab= tiVideo;
}
//---------------------------------------------------------------------------

void __fastcall TfmCalculator::buRetfrClick(TObject *Sender)
{
if(mp->State==TMediaState::Playing)DoStop();
		  tbMain->ActiveTab= tiCheck;
}
//---------------------------------------------------------------------------
void TfmCalculator::DoRefreshList(UnicodeString aPath)
{

//laPath->Text=aPath;
DynamicArray<UnicodeString> x;
TListViewItem *xItem;
x=System::Ioutils::TDirectory::GetFiles(aPath, "*.3gp");
lv->BeginUpdate();
try
{
	for(int i(0);i<x.Length;i++)
{
	  xItem=lv->Items->Add();
	  xItem->Text=System::Ioutils::TPath::GetFileNameWithoutExtension(x[i]);
	  xItem->Detail=x[i];
	}
	}
	__finally
	{
	  lv->EndUpdate();
	}

}
void TfmCalculator::DoStop()
{
 mp->Stop();
tm->Enabled=false;
lv->ItemIndex=-1;
tbVideoChanging->Value=0;
FPauseTime=0;
}
void __fastcall TfmCalculator::tmTimer(TObject *Sender)
{
tbVideoChanging->Tag=1;
tbVideoChanging->Value=mp->CurrentTime;
tbVideoChanging->Tag=0;
laTime->Text=Format("%2.2d:%2.2d",ARRAYOFCONST((
(int)mp->CurrentTime/MediaTimeScale/60,
(int)mp->CurrentTime/MediaTimeScale%60
)));
}
//---------------------------------------------------------------------------

void __fastcall TfmCalculator::buPlayClick(TObject *Sender)
{
	 if (lv->ItemIndex!=-1)
	{
		tm->Enabled=false;
		mp->Clear();
		mp->FileName=lv->Items->AppearanceItem[lv->ItemIndex]->Detail;
		tbVideoChanging->Enabled=true;
		tbVideoChanging->Max=mp->Duration;
		laTimeConst->Text=Format("%2.2d;%2.2d",ARRAYOFCONST
		(( (unsigned int)mp->Duration/MediaTimeScale/60,
			(unsigned int)mp->Duration /MediaTimeScale%60
			)));
			mp->CurrentTime=FPauseTime;
			FPauseTime=0;
			mp->Play();
			tm->Enabled=true;
	}
}
//---------------------------------------------------------------------------

void __fastcall TfmCalculator::buPauseClick(TObject *Sender)
{
if(mp->State==TMediaState::Playing)
{
FPauseTime=mp->Media->CurrentTime;
mp->Stop();
}
else
{
  buPlayClick(this);
}
}
//---------------------------------------------------------------------------

void __fastcall TfmCalculator::tbVideoChangingChange(TObject *Sender)
{
if(tbVideoChanging->Tag==0)
{
if((mp->State==TMediaState::Stopped)&&(FPauseTime!=0))
{
FPauseTime=tbVideoChanging->Value;
} else
{
mp->CurrentTime=tbVideoChanging->Value;
}


}

}
//---------------------------------------------------------------------------

void __fastcall TfmCalculator::buStopClick(TObject *Sender)
{
if(mp->State==TMediaState::Playing)DoStop();
}
//---------------------------------------------------------------------------

void __fastcall TfmCalculator::lvItemClick(TObject * const Sender, TListViewItem * const AItem)

{
  buPlayClick(this);
}
//---------------------------------------------------------------------------

