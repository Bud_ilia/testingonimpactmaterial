//---------------------------------------------------------------------------

#ifndef fmuKnowledgeH
#define fmuKnowledgeH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.ListBox.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.ImgList.hpp>
#include <FMX.Objects.hpp>
#include <System.Ioutils.hpp>
#include <FMX.Media.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <System.Ioutils.hpp>
#include <FMX.Gestures.hpp>
//---------------------------------------------------------------------------
const UnicodeString cWay =  "..\\..\\Video\\";
const UnicodeString mp4="*.mp4" ;
const UnicodeString	gp3="*.3gp";
class TfmKnowledge : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tiMenu;
	TTabItem *tiCheckingKnowledge;
	TTabItem *tiKnowledge;
	TToolBar *tbMenu;
	TLabel *laCaptionMenu;
	TButton *buGoAway;
	TLayout *ly;
	TListBox *lb;
	TListBoxItem *lbiKnowledge;
	TListBoxItem *lbiChecking;
	TToolBar *tbKnowledge;
	TLabel *laKnowledgeCaption;
	TButton *buFromKnowledge;
	TLayout *lyTest;
	TTabControl *tcTest;
	TTabItem *tiBegginning;
	TLabel *laCaptionBegginning;
	TButton *buBegin;
	TTabItem *tiQuest1;
	TTabItem *tiQuest2;
	TTabItem *tiQuest3;
	TTabItem *laQuest4;
	TTabItem *tiQuest5;
	TTabItem *laQuest6;
	TTabItem *tiQuest7;
	TTabItem *tiQuest8;
	TTabItem *tiQuest9;
	TTabItem *tiQuest10;
	TTabItem *tiEnd;
	TLabel *laQuest1Caption;
	TLabel *laQuestCaption2;
	TLabel *laQuestCaption3;
	TLabel *laQuestCaption4;
	TLabel *laQuestCaption5;
	TLabel *laQuestCaption6;
	TLabel *laQuestCaption7;
	TLabel *laQuestCaption8;
	TLabel *laQuestCaption9;
	TLabel *laQuestCaption10;
	TMemo *mEnd;
	TGridPanelLayout *gplQuestion1;
	TButton *buQuestion1Answ1;
	TButton *buQuestion1Answ2;
	TButton *buQuestion1Answ3;
	TGridPanelLayout *gplQuestion2;
	TButton *buQuestion2Answ1;
	TButton *buQuestion2Answ2;
	TButton *buQuestion2Answ3;
	TGridPanelLayout *gplQuestion3;
	TButton *buQuestion3Answ1;
	TButton *buQuestion3Answ2;
	TButton *buQuestion3Answ3;
	TGridPanelLayout *gplQuestion4;
	TButton *buQuestion4Answ1;
	TButton *buQuestion4Answ2;
	TButton *buQuestion4Answ3;
	TGridPanelLayout *gplQuestion5;
	TButton *buQuestion5Answ1;
	TButton *buQuestion5Answ2;
	TButton *buQuestion5Answ3;
	TGridPanelLayout *gplQuestion6;
	TButton *buQuestion6Answ1;
	TButton *buQuestion6Answ2;
	TButton *buQuestion6Answ3;
	TGridPanelLayout *gplQuestion7;
	TButton *buQuestion7Answ1;
	TButton *buQuestion7Answ2;
	TButton *buQuestion7Answ3;
	TGridPanelLayout *gplQuestion8;
	TButton *buQuestion8Answ1;
	TButton *buQuestion8Answ2;
	TButton *buQuestion8Answ3;
	TGridPanelLayout *gplQuestion9;
	TButton *buQuestion9Answ1;
	TButton *buQuestion9Answ2;
	TButton *buQuestion9Answ3;
	TGridPanelLayout *gplQuestion10;
	TButton *buQuestion10Answ1;
	TButton *buQuestion10Answ2;
	TButton *buQuestion10Answ3;
	TToolBar *ToolBar1;
	TLabel *laEndCaption;
	TButton *buRepeatTest;
	TButton *buEndReturn;
	TButton *buGoAvay;
	TScrollBox *sbKnowledge;
	TGroupBox *gbPictures;
	TGlyph *gImage;
	TToolBar *tbOperatePictures;
	TButton *buForwards;
	TButton *buBackwards;
	TLabel *laInfo;
	TText *tInfo;
	TListBoxItem *lbiVideo;
	TTabItem *tiVideo;
	TToolBar *tbVideo;
	TLabel *laCaptionVideo;
	TButton *buVideoFrom;
	TMemo *mAns_1;
	TMemo *mQ2;
	TMemo *mQ_3;
	TMemo *mQ_4;
	TMemo *Q_5;
	TMemo *Q_6;
	TMemo *Q7;
	TMemo *Q_8;
	TMemo *Q_9;
	TMemo *q_10;
	TToolBar *tbBottom;
	TButton *buPrevious;
	TButton *buNext;
	TButton *buPause;
	TButton *buContinue;
	TButton *buStop;
	TLayout *lyVideoPlayer;
	TListView *lv;
	TMediaPlayerControl *mpc;
	TLayout *lyPlayTime;
	TLabel *laTimeChanged;
	TLabel *laTimeVideo;
	TTrackBar *tbChengingProcess;
	TButton *buRefreshList;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buGoAwayClick(TObject *Sender);
	void __fastcall lbiKnowledgeClick(TObject *Sender);
	void __fastcall lbiCheckingClick(TObject *Sender);
	void __fastcall ReturnToMenu(TObject *Sender);
	void __fastcall buBeginClick(TObject *Sender);
	void __fastcall buRepeatTestClick(TObject *Sender);
	void __fastcall buBackwardsClick(TObject *Sender);
	void __fastcall buForwardsClick(TObject *Sender);
	void __fastcall lbiVideoClick(TObject *Sender);
	void __fastcall buQuestion1Answ1Click(TObject *Sender);
	void __fastcall buQuestion1Answ2Click(TObject *Sender);
	void __fastcall buQuestion2Answ1Click(TObject *Sender);
	void __fastcall buQuestion2Answ2Click(TObject *Sender);
	void __fastcall buQuestion3Answ3Click(TObject *Sender);
	void __fastcall buQuestion3Answ1Click(TObject *Sender);
	void __fastcall buQuestion4Answ1Click(TObject *Sender);
	void __fastcall buQuestion4Answ2Click(TObject *Sender);
	void __fastcall buQuestion5Answ1Click(TObject *Sender);
	void __fastcall buQuestion5Answ2Click(TObject *Sender);
	void __fastcall buQuestion6Answ1Click(TObject *Sender);
	void __fastcall buQuestion6Answ2Click(TObject *Sender);
	void __fastcall buQuestion7Answ2Click(TObject *Sender);
	void __fastcall buQuestion7Answ1Click(TObject *Sender);
	void __fastcall buQuestion8Answ1Click(TObject *Sender);
	void __fastcall buQuestion8Answ3Click(TObject *Sender);
	void __fastcall buQuestion9Answ3Click(TObject *Sender);
	void __fastcall buQuestion9Answ1Click(TObject *Sender);
	void __fastcall buQuestion10Answ1Click(TObject *Sender);
	void __fastcall buQuestion10Answ2Click(TObject *Sender);
	void __fastcall buRefreshListClick(TObject *Sender);
	void __fastcall buStopClick(TObject *Sender);
	void __fastcall buPauseClick(TObject *Sender);
	void __fastcall buPlayClick(TObject *Sender);
	void __fastcall tbChengingProcessChange(TObject *Sender);
	void __fastcall lvItemClick(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall buPreviousClick(TObject *Sender);
	void __fastcall buNextClick(TObject *Sender);

private:
	UnicodeString FPath;
	void DoRefreshList(UnicodeString aPath,const UnicodeString Sort);
	unsigned int FPauseTime;
void LoadItem(int aIndex);
void DoStop();
public:		// User declarations
	__fastcall TfmKnowledge(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfmKnowledge *fmKnowledge;
//---------------------------------------------------------------------------
#endif
